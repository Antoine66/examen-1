<?php


/**
 * Class Activite
 * Cette classe sert de modèle pour la création des activitées.
 */
class Activite
{
    /**
     * @var string
     */
    private string $lieu;
    /**
     * @var datetime
     */
    private datetime $date;
    /**
     * @var string
     */
    private string $partenaires;
    /**
     * @var string
     */
    private string $activite;
    /**
     * @var string
     */
    private string $determinants;
    /**
     * @var string
     */
    private string $intensite;
    /**
     * @var int
     */
    private int $duree;
    /**
     * @var string
     */
    private string $effet;
    /**
     * @var string
     */
    private string $motivation;
    /**
     * @var int
     */
    private int $plaisir;
    /**
     * @var array
     */
    private array $Activites;

    /**
     * @return array
     */
    public function getActivites(): array
    {
        return $this->Activites;
    }

    /**
     * @param array $Activites
     */
    public function setActivites(array $Activites): void
    {
        $this->Activites = $Activites;
    }

    /**
     * Constructeur des propriétées pour la classse Activite
     * Activite constructor.
     * @param array $activiteDetails
     */
    public function __construct(array $activiteDetails, $Activites=[])
    {
        foreach ($activiteDetails as $detail => $valeur) {
            if (property_exists($this, $detail)) {
                $this->$detail = $valeur;
            }

        }
        $this->Activites=$Activites;
    }

    /**
     * @return string
     */
    public function getLieu(): string
    {
        return $this->lieu;
    }

    /**
     * @param string $lieu
     */
    public function setLieu(string $lieu): void
    {
        $this->lieu = $lieu;
    }

    /**
     * @return datetime
     */
    public function getDate(): datetime
    {
        return $this->date;
    }

    /**
     * @param datetime $date
     */
    public function setDate(datetime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getPartenaires(): string
    {
        return $this->partenaires;
    }

    /**
     * @param string $partenaires
     */
    public function setPartenaires(string $partenaires): void
    {
        $this->partenaires = $partenaires;
    }

    /**
     * @return string
     */
    public function getActivite(): string
    {
        return $this->activite;
    }

    /**
     * @param string $activite
     */
    public function setActivite(string $activite): void
    {
        $this->activite = $activite;
    }

    /**
     * @return string
     */
    public function getDeterminants(): string
    {
        return $this->determinants;
    }

    /**
     * @param string $determinants
     */
    public function setDeterminants(string $determinants): void
    {
        $this->determinants = $determinants;
    }

    /**
     * @return string
     */
    public function getIntensite(): string
    {
        return $this->intensite;
    }

    /**
     * @param string $intensite
     */
    public function setIntensite(string $intensite): void
    {
        $this->intensite = $intensite;
    }

    /**
     * @return int
     */
    public function getDuree(): int
    {
        return $this->duree;
    }

    /**
     * @param int $duree
     */
    public function setDuree(int $duree): void
    {
        $this->duree = $duree;
    }

    /**
     * @return string
     */
    public function getEffet(): string
    {
        return $this->effet;
    }

    /**
     * @param string $effet
     */
    public function setEffet(string $effet): void
    {
        $this->effet = $effet;
    }

    /**
     * @return string
     */
    public function getMotivation(): string
    {
        return $this->motivation;
    }

    /**
     * @param string $motivation
     */
    public function setMotivation(string $motivation): void
    {
        $this->motivation = $motivation;
    }

    /**
     * @return int
     */
    public function getPlaisir(): int
    {
        return $this->plaisir;
    }

    /**
     * @param int $plaisir
     */
    public function setPlaisir(int $plaisir): void
    {
        $this->plaisir = $plaisir;
    }
    public function addActivite(Activite $activite):bool
    {
        array_push( $this->getActivites(), $activite);
        return count($this->getActivites())>0;
    }

}