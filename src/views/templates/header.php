<?php
$titre = "Journal des activités"
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <title><?php echo $titre ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php echo $titre ?>">
    <!-- Bootstrap et Jquery-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous" defer></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous" defer></script>
    <!-- Font awsome pour les étoiles -->
    <script src="https://kit.fontawesome.com/23484f31a4.js" integrity="sha384-mLE5MAuxf6VNHi4Jxt7u2AwvStsr+L6D+37scFIz9KU2yvwi6b1yjdygPIyIf7t9" crossorigin="anonymous" defer></script>
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar">
            <ul class="navbar-nav w-100">
                <li class="nav-item">
                    <a class="nav-link" href="../../index.php">Accueil</a>
                </li>
                <li class="nav-item active dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarCoursDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Mon journal
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarCoursDropdown">
                        <a class="dropdown-item" href="#">Cours 1 (en cours)</a>
                        <a class="dropdown-item disabled" href="#">Cours 2</a>
                        <a class="dropdown-item disabled" href="#">Cours 3</a>
                    </div>
                </li>
                <li class="nav-item dropdown ml-md-auto">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarProfileDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Mon profil
                    </a>
                    <div class="dropdown-menu dropdown-menu-md-right" aria-labelledby="navbarProfileDropdown">
                        <a class="dropdown-item" href="#">Voir mon profil</a>
                        <a class="dropdown-item disabled" href="#">Ajouter un cours</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>
