<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once __DIR__ . "/../models/Activite.php";

/**
 * Class creerActivite
 * Cette classe valide coté serveur les informations puis crée et ajoute une activité au tableau d'activitées
 */
class creerActivite
{
    /**
     * Cette fonction retourne un tableau d'activitées (il en crée un s'il n'existe pas)
     * @return array
     */
    private static function getActivites(): array
    {
        // Créer une nouveau tableau qui contiendra les activités s'il n'existe pas
        if (!isset($_SESSION["activites"])) {
            $_SESSION["activites"] = [];
        }
        return $_SESSION["activites"];
    }

    /*
    Critères de validation :
    - Accepter lettres, ",", ";" "-" "'" pour les champs texte */
    /**
     * Cette fonction valide que les champs de texte contienne des lettres et des caractères mais pas des numéros
     * @param string $chaine
     * @return bool
     */
    private static function estValide(string $chaine): bool
    {
        if (preg_match("^[\WA-z]{1,}$", $chaine)) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Cette Fonction valide l'activité avant de l'ajouter au tableau d'activitées
     * @param array $details
     * @return bool
     */
    public static function ajoutNouvelleActivite(array $details): bool
    {
        $creation = false;
        //validation de l'activité
        if (self::estValide())
        {
            //Si valide, créer l'activité
            $activite = new Activite($details);
            $creation = self::getActivites()->addActivite($activite);
        }
        //Retourner vrai si l'activité a été ajoutée, faux sinon
        return $creation;
    }
}