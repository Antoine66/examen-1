<?php
require_once __DIR__ . "/../../src/controllers/creerActivite.php";

use PHPUnit\Framework\TestCase;

class creerActiviteTest extends TestCase
{
    static function setUpBeforeClass(): void
    {
        $_SESSION = array();
    }
    //test pour estvalide doivent retourner vrai
    public function testEstValideChaineValide():void
    {
        $str = "Nathalie";
        $str1 = "Jean-Marc";
        $str2 = "‘tite laine";
        $this->assertEquals(true, creerActivite::estValide($str));
        $this->assertEquals(true, creerActivite::estValide($str1));
        $this->assertEquals(true, creerActivite::estValide($str2));

    }

    //test pour estvalide doit retourner faux
    public function testEstValideChaineInValide():void
    {
        $str3 = "33 Cité-des-Jeunes";

        $this->assertEquals(false, creerActivite::estValide($str3));
    }
    public function tearDown(): void
    {
        $_SESSION = array();
    }
}
